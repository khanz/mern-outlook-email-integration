// Add here the endpoints for MS Graph API services you would like to use.
const graphConfig = {
    graphMeEndpoint: "https://graph.microsoft.com/v1.0/me",
    graphMailEndpoint: "https://graph.microsoft.com/v1.0/me/messages",
    // graphCreateReplyEndpoint: "https://graph.microsoft.com/v1.0/me/messages/AQMkADAwATM3ZmYAZS00ZjQwLTQ2NGEtMDACLTAwCgBGAAADYL6te_AQUUmeibQTesbKXwcAHsJKIVhCtkK0TS-TtElxeQAAAgEMAAAAHsJKIVhCtkK0TS-TtElxeQAAAAHTg4wAAAA=/createReply",
    graphCreateReplyEndpoint: "https://graph.microsoft.com/v1.0/me/messages/AQMkADAwATM3ZmYAZS00ZjQwLTQ2NGEtMDACLTAwCgBGAAADYL6te_AQUUmeibQTesbKXwcAHsJKIVhCtkK0TS-TtElxeQAAAgEMAAAAHsJKIVhCtkK0TS-TtElxeQAAAAHTg48AAAA=/createReplyAll",
    // graphCreateReplyMessageEndpoint: "https://graph.microsoft.com/v1.0/me/messages/AQMkADAwATM3ZmYAZS00ZjQwLTQ2NGEtMDACLTAwCgBGAAADYL6te_AQUUmeibQTesbKXwcAHsJKIVhCtkK0TS-TtElxeQAAAgEMAAAAHsJKIVhCtkK0TS-TtElxeQAAAgVRAAAA/reply"
    graphCreateReplyMessageEndpoint: "https://graph.microsoft.com/v1.0/me/messages/AQMkADAwATM3ZmYAZS00ZjQwLTQ2NGEtMDACLTAwCgBGAAADYL6te_AQUUmeibQTesbKXwcAHsJKIVhCtkK0TS-TtElxeQAAAgEMAAAAHsJKIVhCtkK0TS-TtElxeQAAAAHTg48AAAA=/replyAll"
    // graphMailEndpoint: "https://graph.microsoft.com/v1.0/me/mailfolders/inbox/messages"
};